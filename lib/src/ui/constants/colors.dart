import 'package:flutter/material.dart';

// const dashBoardColor = Color.fromRGBO(105, 106, 183, 1);
// const splashScreenColorBottom = Color.fromRGBO(105, 106, 183, 1);
// const splashScreenColorTop= Color.fromRGBO(105, 106, 183, 1);
// const appbarcolor=Color.fromRGBO(51, 51, 102, 1);
// const leaveCardcolor =  Color(0xFF8685E5);

const dashBoardColor = Color(0xff6dcbdd);
const splashScreenColorBottom = Color(0xff6dcbdd);
const splashScreenColorTop= Color(0xff6dcbdd);
const appbarcolor=Color(0xff424B54);
const leaveCardcolor =  Color(0xff004E89);